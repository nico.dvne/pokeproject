﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PokeProject.Datas
{
    public interface IObject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<PokeTypes> Types { get; set; }
        public string Description { get; set; }
    }
}
