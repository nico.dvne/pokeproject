﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PokeProject.Datas
{
    public class Pokemon : IObject
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("types")]
        public List<PokeTypes> Types { get; set; }

        public string Description { get; set; }

        public override string ToString()
        {
            string to_string = $"Id = {Id}\n Name = {Name}\n Description = {Description}\n  Types = ";
            foreach(PokeTypes types in Types)
            {
                to_string += types.Type + " / ";
            }

            return to_string;
        }

    }
}
