﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace PokeProject.RecuperationDonnees
{
    public class ApiInteract
    {
        public static async System.Threading.Tasks.Task<string> GetIdNameTypeAsync(int pokeId)
        {
            try
            {
                using (var v_HttpClient = new HttpClient())
                {
                    HttpResponseMessage response;
                    response = await v_HttpClient.GetAsync("https://pokeapi.co/api/v2/pokemon/" + pokeId);
                    string contentResponse = await response.Content.ReadAsStringAsync();

                    return contentResponse;
                }
            }
            catch (Exception)
            {
                return "";
            }                
        }

        public static async System.Threading.Tasks.Task<string> GetDescriptionAsync(int pokeId)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    HttpResponseMessage message = await httpClient.GetAsync("https://pokeapi.co/api/v2/pokemon-species/" + pokeId);
                    message.EnsureSuccessStatusCode();
                    JObject jObject = JsonConvert.DeserializeObject<JObject>(await message.Content.ReadAsStringAsync());
                    string description = jObject.Value<JArray>("flavor_text_entries").First.Value<string>("flavor_text");

                    return description;
                }
            }
            catch(Exception)
            {
                return "";
            }
        }
    }
}
