﻿using PokeProject.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeProject.App
{
    class Affichage
    {
        public static void AffichageTitre()
        {
            Console.WriteLine("| | | | | | | | | | | | |");
            Console.WriteLine("| | | | |       | | | | |");
            Console.WriteLine("| | | |           | | | |");
            Console.WriteLine("| | |               | | |");
            Console.WriteLine("| |                   | |");
            Console.WriteLine("|    POKEMON PROJECT    |");            
            Console.WriteLine("| |                   | |");
            Console.WriteLine("| | |               | | |");
            Console.WriteLine("| | | |           | | | |");
            Console.WriteLine("| | | | |       | | | | |");
            Console.WriteLine("| | | | | | | | | | | | |");
        }

        public static int ValidateUserChoice(List<int> possibilities)
        {
            bool isOK = false;
            int choice = 0;
            do
            {
                try
                {
                    choice = int.Parse(Console.ReadLine());
                    if (possibilities.Contains(choice))
                        isOK = true;
                    else
                        throw new Exception();
                }
                catch (Exception)
                {
                    Console.WriteLine("Saississez à nouveau votre choix :  ");
                }
            } while (!isOK);

            return choice;
        }

        public static int AffichageMenuChoice()
        {
            Console.WriteLine();
            Console.WriteLine("| | | | | | | | | | | | |");
            Console.WriteLine("|                       |");
            Console.WriteLine("| Menu :                |");
            Console.WriteLine("|                       |");
            Console.WriteLine("| 1.Tous les Pokemons   |");
            Console.WriteLine("| 2. Voir un Pokemon    |");
            Console.WriteLine("|                       |");
            Console.WriteLine("|Saississez votre choix |");
            Console.WriteLine("|                       |");
            Console.WriteLine("| | | | | | | | | | | | |");

            return ValidateUserChoice(new List<int>{ 1, 2 });
        }

        public static void AffichageAllIObjects(List<IObject> pokemons)
        {
            foreach(IObject pokemon in pokemons)
            {
                Console.WriteLine($"Id = {pokemon.Id}, Name = {pokemon.Name}");
            }
        }

        public static void ResearchPoke(List<IObject> list)
        {
            Console.WriteLine("| | | | | | | | | | | | |");
            Console.WriteLine("|                       |");
            Console.WriteLine("| Veuillez saisir le    |");
            Console.WriteLine("|  nom ou l'id          |");
            Console.WriteLine("|  du pokemon           |");
            Console.WriteLine("|  recherché     :      |");
            Console.WriteLine("|                       |");
            Console.WriteLine("| | | | | | | | | | | | |");

            string pokename = Console.ReadLine();

            IObject pokeResearch = null;

            if (MainMethods.IsDigit(pokename))
            {
                int pokeId = int.Parse(pokename);
                if (pokeId > 0)
                    pokeResearch = MainMethods.GetIObjectById(list, pokeId); ;
            }
            else
            {
                pokeResearch = MainMethods.GetIObjectByName(list, pokename);
            }


            if(null == pokeResearch)
            {
                Console.Clear();
                Console.WriteLine("Merci de saisir un ID ou un nom valide");
                Console.ReadKey();
                Console.Clear();
                ResearchPoke(list);
            }
            else
            {
                PokemonPage(pokeResearch);
            }

            
        }

        public static void PokemonPage(IObject pokemon)
        {
            Console.WriteLine("-------------------------");
            Console.WriteLine(pokemon.ToString());
            Console.WriteLine("-------------------------");
        }

        public static void AfficheGlobal(List<IObject> pokelist)
        {
            int menu_choice = AffichageMenuChoice();
            if (menu_choice == 1)
                AffichageAllIObjects(pokelist);

            if (menu_choice == 2)
                ResearchPoke(pokelist);
        }
    }
}
