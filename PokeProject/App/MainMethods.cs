﻿using Newtonsoft.Json;
using PokeProject.Datas;
using PokeProject.RecuperationDonnees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PokeProject.App
{
    public class MainMethods
    {
        
        public static async System.Threading.Tasks.Task<List<IObject>> GetObjectsAsync(int howMuch)
        {
            List<IObject> generic = new List<IObject>();
            for(int i = 1; i <= howMuch; i++)
            {
                string contentResponse = await ApiInteract.GetIdNameTypeAsync(i);
                if ("" == contentResponse)
                    continue;

                IObject pokemon = JsonConvert.DeserializeObject<Pokemon>(contentResponse);
                pokemon.Description = await ApiInteract.GetDescriptionAsync(i);
                
                generic.Add(pokemon);
            }

            return generic;
        }

        public static IObject GetIObjectByName(List<IObject> pokelist, string pokename)
        {
            IObject[] pokemons = pokelist.ToArray();

            IEnumerable<IObject> pokemon = from poke in pokemons
                                           where poke.Name == pokename
                                           select poke;

            return pokemon.FirstOrDefault();
        }

        public static IObject GetIObjectById(List<IObject> pokelist, int pokeId)
        {
            IObject[] pokemons = pokelist.ToArray();

            IEnumerable<IObject> pokemon = from poke in pokemons
                                           where poke.Id == pokeId
                                           select poke;

            return pokemon.FirstOrDefault();
        }

        public static bool IsDigit(string to_test)
        {
            foreach(char cara in to_test)
            {
                if (!Char.IsDigit(cara))
                    return false;
            }

            return true;
        }
    }
}
