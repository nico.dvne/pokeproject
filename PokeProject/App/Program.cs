﻿using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using PokeProject.App;
using PokeProject.Datas;
using PokeProject.RecuperationDonnees;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace PokeProject
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            List<IObject> pokemons = new List<IObject>();
            Affichage.AffichageTitre();
            Console.WriteLine("Merci d'attendre tant que nous collectons les données ...");
            pokemons = await MainMethods.GetObjectsAsync(50);
            Console.Clear();
            Affichage.AfficheGlobal(pokemons);
            Console.ReadKey();
            Console.Clear();
        }
    }
}

