using Xunit;
using PokeProject.RecuperationDonnees;

namespace TestUnitaire
{
    public class ApiInteractTest
    {
        [Fact]
        public async System.Threading.Tasks.Task TestGetPokemonAsync()
        {
            string expected = await ApiInteract.GetIdNameTypeAsync(2);

            Assert.NotNull(expected);
        }
    }
}
