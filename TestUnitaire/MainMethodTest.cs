﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using PokeProject.Datas;
using PokeProject.App;

namespace TestUnitaire
{
    public class MainMethodTest
    {
        [Fact]
        public static void TestGetIObjectByName()
        {
            List<IObject> pokeList = new List<IObject>();
            Pokemon pokemon1 = new Pokemon();
            pokemon1.Name = "poketest";
            pokeList.Add(pokemon1);

            IObject retourTest = MainMethods.GetIObjectByName(pokeList, "poketest");

            Assert.Equal(pokemon1, retourTest);

            IObject test2 = MainMethods.GetIObjectByName(pokeList, "random_name");

            Assert.Null(test2);
        }

        [Fact]
        public static void TestGetPokemonById()
        {
            List<IObject> pokeList = new List<IObject>();
            Pokemon pokemon1 = new Pokemon();
            pokemon1.Id = 1;
            pokeList.Add(pokemon1);

            IObject retourTest = MainMethods.GetIObjectById(pokeList, 1);

            Assert.Equal(pokemon1, retourTest);

            IObject test2 = MainMethods.GetIObjectById(pokeList, 10);

            Assert.Null(test2);
        }
    }
}
